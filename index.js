const PDF = require('./src/index.vue');
const pdfjsLib = require('pdfjs-dist');

module.exports = {
  install: function(_vue, options = {}) {
    pdfjsLib.PDFJS.workerSrc = options.workerSrc || '';
    _vue.component('pdf-lab', PDF);
  },
};
