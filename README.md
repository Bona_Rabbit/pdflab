# PDFLab

[![build status](https://gitlab.com/samrose3/pdflab/badges/master/build.svg)](https://gitlab.com/samrose3/pdflab/commits/master)
[![coverage report](https://gitlab.com/samrose3/pdflab/badges/master/coverage.svg)](https://gitlab.com/samrose3/pdflab/commits/master)

PDFLab is a Vue component that makes it easy to render PDF files. It's as simple as
passing in the URL of the PDF or the file data and the component renders.

## Usage

Install the plugin by adding the `pdflab.js` and `pdf.worker.js` files and then install the plugin

```javascript
Vue.use(PDFLab, { workerScr: 'path/to/pdf.worker.js' });
```

Then you can instantiate the component by just doing `<pdf-lab />`

```html
<pdf-lab :pdf="pdf" />
```

The only required props is `pdf` and this is the PDF file URL or raw text read from the PDF you want to
display. There are also on load and on error events you an listen to.

## Example

A simple example that shows how the PDF can be fetched with a URL.

```javascript
import PDFLab from 'pdflab';

new Vue({
  el: '#js-test',
  template: `
    <pdf-lab
      :pdf="pdf"
      @pdflabload="done"
      @pdflaberror="error" />
  `,
  data() {
    return {
      pdf: 'path/to/my-file.pdf'
    };
  },
  methods: {
    done: function() {
      console.log('PDF has been loaded');
    },
    error: function(error) {
      console.error(error.message);
    }
  }
});
```
