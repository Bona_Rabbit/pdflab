module.exports = (config) => {
  config.set({
    browsers: ['PhantomJS'],
    frameworks: ['jasmine'],
    files: ['./test/index.js'],
    reporters: ['spec', 'coverage'],
    preprocessors: {
      './test/index.js': ['webpack'],
    },
    singleRun: true,
    webpack: {
      module: {
        loaders: [{
          test: /\.vue$/,
          use: ['vue-loader'],
          exclude: /node_modules/,
        }, {
          test: /\.js$/,
          use: ['babel-loader'],
          exclude: /node_modules/,
        },{
          test: /\.gif$/,
          loader: 'url-loader',
          query: { mimetype: 'image/gif' },
        }, {
          test: /\.(worker.js|pdf)$/,
          loader: 'file-loader',
          exclude: /node_modules/,
        }],
      },
    },
    webpackMiddleware: {
      noInfo: true,
    },
    coverageReporter: {
      dir: './coverage',
      reporters: [
        { type: 'lcov', subdir: '.' },
        { type: 'text-summary' },
      ],
    },
  });
};
